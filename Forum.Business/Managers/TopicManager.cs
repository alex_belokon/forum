﻿using Forum.Core.Interfaces.Managers;
using Forum.Core.Interfaces.Repositories;
using Forum.Core.Interfaces.UnitOfWork;
using Forum.Core.Models;
using System;
using System.Linq;


namespace Forum.Business.Managers
{
    public class TopicManager : ITopicManager
    {
        private readonly ITopicRepository _topicRepository;
        private readonly IUnitOfWork _unitOfWork;

        public TopicManager(IUnitOfWork unitOfWork,
                              ITopicRepository topicRepository)
        {
            _topicRepository = topicRepository;
            _unitOfWork = unitOfWork;
        }

        public Topic[] GetAllTopics()
        {
            return _topicRepository.SelectAll().ToArray();
        }

        public Topic GetTopicById(int topicId)
        {
            return _topicRepository.Find(topicId);
        }

        public void AddTopic(string topicName, string topicText, DateTime topicDateTime, int userId)
        {
            var topic = new Topic
            {
                TopicName = topicName,
                TopicText = topicText,
                TopicDateTime = topicDateTime,
                UserId = userId
            };

            _topicRepository.Insert(topic);
            _unitOfWork.Save();
        }

        public void EditTopic(int topicId, string topicName, string topicText, DateTime topicDateTime, int userId)
        {
            var record = _topicRepository.Get(topicId);
            if (record != null)
            {
                record.TopicName = topicName;
                record.TopicText = topicText;
                record.TopicDateTime = topicDateTime;
                record.UserId = userId;
                _unitOfWork.Save();
            }
        }
        public void DeleteTopic(int topicId)
        {
            var record = _topicRepository.Find(topicId);
            if (record != null)
            {
                _topicRepository.Delete(record);
                _unitOfWork.Save();
            }
        }

    }
}
