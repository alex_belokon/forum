﻿using Forum.Core.Interfaces.Managers;
using Forum.Core.Interfaces.Repositories;
using Forum.Core.Interfaces.UnitOfWork;
using Forum.Core.Models;
using System;
using System.Linq;


namespace Forum.Business.Managers
{
    public class MessageManager : IMessageManager
    {
        private readonly IMessageRepository _messageRepository;
        private readonly IUnitOfWork _unitOfWork;

        public MessageManager(IUnitOfWork unitOfWork,
                              IMessageRepository messageRepository)
        {
            _messageRepository = messageRepository;
            _unitOfWork = unitOfWork;
        }

        public Message[] GetAllMessages()
        {
            return _messageRepository.SelectAll().ToArray();
        }

        public Message GetMessageById(int messageId)
        {
            return _messageRepository.Find(messageId);
        }

        public void AddMessage(string messageText, DateTime messageDateTime, int topicId, int userId)
        {
            var message = new Message
            {
                MessageText = messageText,
                MessageDateTime = messageDateTime,
                TopicId = topicId,
                UserId = userId
            };

            _messageRepository.Insert(message);
            _unitOfWork.Save();
        }

        public void EditMessage(int messageId, string messageText, DateTime messageDateTime, int topicId, int userId)
        {
            var record = _messageRepository.Get(messageId);
            if (record != null)
            {
                record.MessageText = messageText;
                record.MessageDateTime = messageDateTime;
                record.TopicId = topicId;
                record.UserId = userId;
                _unitOfWork.Save();
            }
        }
        public void DeleteMessage(int messageId)
        {
            var record = _messageRepository.Find(messageId);
            if (record != null)
            {
                _messageRepository.Delete(record);
                _unitOfWork.Save();
            }
        }

    }
}
