﻿using Forum.Core.Interfaces.Managers;
using Forum.Core.Interfaces.Repositories;
using Forum.Core.Interfaces.UnitOfWork;
using Forum.Core.Models;
using System;
using System.Linq;


namespace Forum.Business.Managers
{
    public class UserManager : IUserManager
    {
        private readonly IUserRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UserManager(IUnitOfWork unitOfWork,
                              IUserRepository userRepository)
        {
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
        }

        public User[] GetAllUsers()
        {
            return _userRepository.SelectAll().ToArray();
        }

        public User GetUserById(int userId)
        {
            return _userRepository.Find(userId);
        }

        public int AddUser(string userIdFromIdentity, string userName, string userLogin, string userPass)
        {
            var user = new User
            {
                UserIdFromIdentity = userIdFromIdentity,
                UserName = userName,
                UserLogin = userLogin,
                UserPass = userPass
            };

            _userRepository.Insert(user);
            _unitOfWork.Save();
            return user.UserId;
        }

        public void EditUser(int userId, string userIdFromIdentity, string userName, string userLogin, string userPass)
        {
            var record = _userRepository.Get(userId);
            if (record != null)
            {
                record.UserIdFromIdentity= userIdFromIdentity;
                record.UserName = userName;
                record.UserLogin = userLogin;
                record.UserPass = userPass;
                _unitOfWork.Save();
            }
        }
        public void DeleteUser(int userId)
        {
            var record = _userRepository.Find(userId);
            if (record != null)
            {
                _userRepository.Delete(record);
                _unitOfWork.Save();
            }
        }

    }
}
