﻿CREATE TABLE [dbo].[User] (
    [UserId]             INT            IDENTITY (1, 1) NOT NULL,
    [UserIdFromIdentity] NVARCHAR (MAX) NULL,
    [UserName]           NVARCHAR (MAX) NULL,
    [UserLogin]          NVARCHAR (MAX) NULL,
    [UserPass]           NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.User] PRIMARY KEY CLUSTERED ([UserId] ASC)
);

