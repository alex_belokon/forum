﻿CREATE TABLE [dbo].[Topic] (
    [TopicId]       INT            IDENTITY (1, 1) NOT NULL,
    [TopicName]     NVARCHAR (MAX) NULL,
    [TopicText]     NVARCHAR (MAX) NULL,
    [TopicDateTime] DATETIME       NOT NULL,
    [UserId]        INT            NOT NULL,
    CONSTRAINT [PK_dbo.Topic] PRIMARY KEY CLUSTERED ([TopicId] ASC),
    CONSTRAINT [FK_dbo.Topic_dbo.User_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId])
);


GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[Topic]([UserId] ASC);

