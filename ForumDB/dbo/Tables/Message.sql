﻿CREATE TABLE [dbo].[Message] (
    [MessageId]       INT            IDENTITY (1, 1) NOT NULL,
    [MessageText]     NVARCHAR (MAX) NULL,
    [MessageDateTime] DATETIME       NOT NULL,
    [TopicId]         INT            NOT NULL,
    [UserId]          INT            NOT NULL,
    CONSTRAINT [PK_dbo.Message] PRIMARY KEY CLUSTERED ([MessageId] ASC),
    CONSTRAINT [FK_dbo.Message_dbo.Topic_TopicId] FOREIGN KEY ([TopicId]) REFERENCES [dbo].[Topic] ([TopicId]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.Message_dbo.User_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[Message]([UserId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TopicId]
    ON [dbo].[Message]([TopicId] ASC);

