﻿using Forum.Core.Models;
using System.Data.Entity.ModelConfiguration;

namespace Forum.Data.Mapping
{
    public class TopicMap : EntityTypeConfiguration<Topic>
    {
        public TopicMap()
        {
            // Primary Key
            this.HasKey(t => t.TopicId);

            // Properties


            // Table & Column Mappings
            this.ToTable("Topic");

			// Navigation properties

			this.HasRequired(t => t.Users);

        }
    }
}
