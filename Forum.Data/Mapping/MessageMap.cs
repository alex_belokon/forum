﻿using System.Data.Entity.ModelConfiguration;
using Forum.Core.Models;

namespace Forum.Data.Mapping
{
    public class MessageMap : EntityTypeConfiguration<Message>
    {
        public MessageMap()
        {
            // Primary Key
            this.HasKey(t => t.MessageId);

            // Properties


            // Table & Column Mappings
            this.ToTable("Message");

            // Navigation properties
            this.HasRequired(t => t.Topics);
        }
    }
}
