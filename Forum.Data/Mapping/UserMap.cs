﻿using Forum.Core.Models;
using System.Data.Entity.ModelConfiguration;

namespace Forum.Data.Mapping
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            // Primary Key
            this.HasKey(t => t.UserId);

            // Properties


            // Table & Column Mappings
            this.ToTable("User");

            // Navigation properties
            //this.HasRequired(t => t.Topics).WithMany().HasForeignKey(u => u.UserId);
            this.HasMany(t => t.Topics).WithRequired(t => t.Users).WillCascadeOnDelete(false);
        }
    }
}
