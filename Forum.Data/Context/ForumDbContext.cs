
using Forum.Core.Interfaces;
using Forum.Core.Models;
using Forum.Data.Mapping;
using System.Data.Entity;

namespace Forum.Data.Context
{
    public class ForumDbContext : DbContext, IForumDbContext
    {
        // Your context has been configured to use a 'ForumDbContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'Forum.Data.Context.ForumDbContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'ForumDbContext' 
        // connection string in the application configuration file.
        public ForumDbContext()
            : base("name=ForumDbContext")
        {
            var ensureDLLIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.
        public ForumDbContext(string connectionStringName) : base("name=" + connectionStringName)
        {
            var ensureDLLIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        public virtual DbSet<Topic> Topics { get; set; }
        public virtual DbSet<Message> Messages { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // don't create database at startup
            //Database.SetInitializer(new DropCreateDatabaseAlways<ForumDbContext>());

            modelBuilder.Configurations.Add(new MessageMap());
            modelBuilder.Configurations.Add(new TopicMap());
            modelBuilder.Configurations.Add(new UserMap());

        }
    }

   
}