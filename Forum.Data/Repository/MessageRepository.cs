﻿using Forum.Core.Interfaces.Repositories;
using Forum.Core.Interfaces.UnitOfWork;
using Forum.Core.Models;
using System.Linq;


namespace Forum.Data.Repository
{
    public class MessageRepository: RepositoryBase<Message>, IMessageRepository
    {
        public MessageRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public Message Get(int id)
        {
            return this.Select(x => x.MessageId == id).FirstOrDefault();
        }
    }
}
