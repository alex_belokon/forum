﻿using Forum.Core.Interfaces.Managers;
using Forum.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Forum.Controllers
{
    [Authorize]
    public class MessageController : Controller
    {
        private readonly IMessageManager _messageManager;
        private readonly ITopicManager _topicManager;
        private readonly IUserManager _userManager;


        public MessageController()
        {

        }

        public MessageController(IMessageManager messageManager, IUserManager userManager, ITopicManager topicManager)
        {
            _messageManager = messageManager;
            _topicManager = topicManager;
            _userManager = userManager;
        }
        [AllowAnonymous]
        public ActionResult List(int topicId)
        {
            ViewBag.AuthUser = User.Identity.IsAuthenticated ? this.GetUserId() : 0;
            var messages = _messageManager.GetAllMessages().Where(x => x.TopicId == topicId).OrderBy(x => x.MessageDateTime).ToArray();
            return PartialView("MessageList", messages);
        }

        public ActionResult AddNewMessage(int topicId)
        {
            var userId = _userManager.GetAllUsers().FirstOrDefault(x => x.UserLogin == User.Identity.Name).UserId;

            var model = new MessageModel
            {
                TopicId = topicId,
                UserId = userId
            };

            return PartialView("MessageDetails", model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddNewMessage(MessageModel message)
        {
            if (ModelState.IsValid)
            {
                _messageManager.AddMessage(message.MessageText, message.MessageDateTime, message.TopicId, message.UserId);
                return RedirectToAction("Topic", "Topic", new { Id = message.TopicId });
            }
            return View("MessageDetails", message);
        }

        public PartialViewResult EditMessage(int messageId)
        {
            var model = GetModel(messageId);
            return PartialView("MessageDetails", model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult EditMessage(MessageModel message)
        {
            if (ModelState.IsValid)
            {
                _messageManager.EditMessage(message.MessageId, message.MessageText, message.MessageDateTime, message.TopicId, message.UserId);
                return RedirectToAction("Topic", "Topic", new { Id = message.TopicId });
            }

            return PartialView("MessageDetails", message);
        }

        private MessageModel GetModel(int messageId)
        {
            var message = _messageManager.GetMessageById(messageId);
            var model = new MessageModel
            {
                MessageId = message.MessageId,
                MessageText = message.MessageText,
                MessageDateTime = message.MessageDateTime,
                TopicId = message.TopicId,
                UserId = message.UserId
            };

            return model;
        }


        public ActionResult DeleteMessage(int messageId)
        {
            try
            {

                var model = GetModel(messageId);
                return View(model);
            }

            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult DeleteMessage(MessageModel message)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int Id = message.TopicId;
                    _messageManager.DeleteMessage(message.MessageId);
                    return RedirectToAction("Topic", "Topic", new { Id });
                }
                else
                {
                    return HttpNotFound();
                }

            }

            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
        [NonActionAttribute]
        private int GetUserId()
        {

            int result = _userManager.GetAllUsers().FirstOrDefault(x => x.UserLogin == User.Identity.Name).UserId;
            return result;
        }
    }
}