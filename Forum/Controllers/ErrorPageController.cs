﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Forum.Controllers
{
    public class ErrorPageController : Controller
    {
        // http://benfoster.io/blog/aspnet-mvc-custom-error-pages
        // GET: Error
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Error(int statusCode, Exception exception)
        {
            Response.StatusCode = statusCode;
            ViewBag.StatusCode = statusCode + " Error";
            ViewBag.Exception = exception;

            return View();
        }
    }
}