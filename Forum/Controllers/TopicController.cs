﻿using Forum.Core.Interfaces.Managers;
using Forum.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Forum.Controllers
{
    [Authorize]
    public class TopicController : Controller
    {
        private readonly ITopicManager _topicManager;
        private readonly IUserManager _userManager;

        public TopicController()
        {

        }

        public TopicController(ITopicManager topicManager, IUserManager userManager)
        {
            _topicManager = topicManager;
            _userManager = userManager;
        }
        
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Index(int? userId)
        {
            ViewData["userId"] = userId == null ? 0 : this.GetUserId();
            var topics = _topicManager.GetAllTopics().OrderBy(x => x.TopicDateTime).ToArray();
            return View("TopicList", topics);
        }
        [AllowAnonymous]
        public ActionResult Topic(int id)
        {

            ViewBag.AuthUser =User.Identity.IsAuthenticated ? this.GetUserId() : 0;
            var model = GetModel(id);
            return View("Topic", model);
        }
        public ActionResult AddNewTopic(int userId)
        {
            var model = new TopicModel { UserId = userId != 0 ? userId : this.GetUserId() };
            return View("TopicDetails", model);

        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddNewTopic(TopicModel topic)
        {
            if (!string.IsNullOrEmpty(topic.TopicName))
            {
                var record = _topicManager.GetAllTopics()
                    .FirstOrDefault(x => string.Equals(x.TopicName, topic.TopicName, StringComparison.InvariantCultureIgnoreCase));
                if (record != null) ModelState.AddModelError(nameof(topic.TopicName), $"We already have Topic '{topic.TopicName}' in DB");
            }

            if (ModelState.IsValid)
            {
                _topicManager.AddTopic(topic.TopicName, topic.TopicText, topic.TopicDateTime, topic.UserId);
                return RedirectToAction("Index", topic.UserId);
            }
            return View("TopicDetails", topic);
        }

        public ActionResult EditTopic(int topicId)
        {
            var model = GetModel(topicId);
            return PartialView("TopicDetails", model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult EditTopic(TopicModel topic)
        {
            if (!string.IsNullOrEmpty(topic.TopicName))
            {
                var oldTopic = _topicManager.GetTopicById(topic.TopicId);
                if (!oldTopic.TopicName.Equals(topic.TopicName, StringComparison.InvariantCultureIgnoreCase))
                {
                    var record = _topicManager.GetAllTopics()
                        .FirstOrDefault(x => string.Equals(x.TopicName, topic.TopicName, StringComparison.InvariantCultureIgnoreCase));

                    if (record != null) ModelState.AddModelError(nameof(topic.TopicName), $"We already have topic '{topic.TopicName}' in DB");
                }
            }
            if (ModelState.IsValid)
            {
                _topicManager.EditTopic(topic.TopicId, topic.TopicName, topic.TopicText, topic.TopicDateTime, topic.UserId);
                return RedirectToAction("Topic", new { id = topic.TopicId });
            }

            return PartialView("TopicDetails", topic);
        }

        private TopicModel GetModel(int topicId)
        {
            var topic = _topicManager.GetTopicById(topicId);
            var model = new TopicModel
            {
                TopicId = topic.TopicId,
                TopicName = topic.TopicName,
                TopicText = topic.TopicText,
                TopicDateTime = topic.TopicDateTime,
                UserId = topic.UserId
            };

            return model;
        }


        public ActionResult DeleteTopic(int topicId)
        {
            try
            {

                var model = GetModel(topicId);
                return View(model);
            }

            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult DeleteTopic(TopicModel topic)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _topicManager.DeleteTopic(topic.TopicId);
                    return RedirectToAction("Index");
                }
                else
                {
                    return HttpNotFound();
                }

            }

            catch (Exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
        [NonActionAttribute]
        private int GetUserId()
        {

            int result = _userManager.GetAllUsers().FirstOrDefault(x => x.UserLogin == User.Identity.Name).UserId;
            return result;
        }
    }
}