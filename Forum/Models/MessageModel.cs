﻿using Forum.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Forum.Models
{
    public class MessageModel
    {
        public int MessageId { get; set; }
        [AllowHtml]
        [Required(ErrorMessage = "The Text field cann't be empty")]
        [DisplayName("Message Text")]
        public string MessageText { get; set; }

        public int TopicId { get; set; }

        public DateTime MessageDateTime { get; set; }

        public int UserId { get; set; }

    }
}