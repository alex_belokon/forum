﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Forum.Models
{
    public class TopicModel
    {
        public int TopicId { get; set; }
        [Required(ErrorMessage = "The Name field cann't be empty")]
        [DisplayName("Name")]
        public string TopicName { get; set; }
        [AllowHtml]
        [Required(ErrorMessage = "The Text field cann't be empty")]
        [DisplayName("Topic Text")]
        public string TopicText { get; set; }

        public DateTime TopicDateTime { get; set; }

        public int UserId { get; set; }

    }
}