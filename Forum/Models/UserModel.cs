﻿using Forum.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Forum.Models
{
    public class UserModel
    {
        public int UserId { get; set; }

        [DisplayName("User Name")]
        public string UserName { get; set; }


        public string UserLogin { get; set; }

        public string UserPass { get; set; }

        [HiddenInput(DisplayValue = false)]
        public byte[] AvatarData { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ImageMimeType { get; set; }


    }
}