﻿using Autofac;
using Autofac.Integration.Mvc;
using Forum.Business.Managers;
using Forum.Core.Interfaces.Managers;
using Microsoft.Owin;
using Owin;
using System.Reflection;
using System.Web.Mvc;

[assembly: OwinStartupAttribute(typeof(Forum.Startup))]
namespace Forum
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

            var builder = new ContainerBuilder();

            // Register MVC controllers.
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            //builder.RegisterType<UserManager>().As<IUserManager>().SingleInstance();
            // Register configuration.
            //CoreConfiguration config = (CoreConfiguration)ConfigurationManager.GetSection("CoreConfiguration");
            //builder.Register(c => config)
            //    .AsImplementedInterfaces()
            //    .SingleInstance();

            //Microsoft.ApplicationInsights.Extensibility.TelemetryConfiguration.Active.InstrumentationKey = config.ApplicationInsightsKey;

            Assembly dataAssembly = Assembly.Load("Forum.Data");
            Assembly businessAssembly = Assembly.Load("Forum.Business");
            

            builder.RegisterAssemblyTypes(dataAssembly)
                .Where(t => t.Name.EndsWith("UnitOfWork"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(dataAssembly)
                .Where(t => t.Name.EndsWith("Repository") || t.Name.EndsWith("DbContext"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(businessAssembly)
                .Where(t => t.Name.EndsWith("Manager"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            //builder.RegisterWebApiFilterProvider(GlobalConfiguration.Configuration);

            var container = builder.Build();

            //GlobalConfiguration.Configuration.DependencyResolver = new AutofacDependencyResolver(container);
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            app.UseAutofacMiddleware(container);
        }
    }
}
