﻿using System.Web;
using System.Web.Optimization;

namespace Forum
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/app.js",
                      "~/Scripts/velocity.min.js",
                      "~/Scripts/jquery-ui-1.12.1.js",
                      "~/Scripts/jquery.unobtrusive-ajax.js",
                      "~/Content/summernote-master/dist/summernote.js",
                      "~/Content/summernote-master/dist/summernote-lite.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      //"~/Content/bootstrap.min.css",
                      //"~/Content/bootstrap-grid.css",

                      "~/fonts/icons/main/mainfont/style.css",
                      "~/fonts/icons/font-awesome/css/font-awesome.css",
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap.min.css",
                      "~/Content/bootstrap-grid.css",
                      "~/Content/site.css",
                      "~/Content/style.css",
                      "~/Content/themes/base/jquery-ui.css",
                      "~/Content/summernote-master/dist/summernote.css",
                      "~/Content/summernote-master/dist/summernote-lite.css"));
        }
    }
}
