﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Forum
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
        name: "Default",
        url: "{controller}/{action}/{userId}",
        defaults: new { controller = "Topic", action = "Index", userId = UrlParameter.Optional }
        );

            routes.MapRoute(
                  name: "Topic",
                  url: "{controller}/{action}/{Id}",
                  defaults: new { controller = "Topic", action = "Topic", Id = UrlParameter.Optional }
              );


        }
    }
}
