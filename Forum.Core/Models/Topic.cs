﻿using System;
using System.Collections.Generic;

namespace Forum.Core.Models
{
    public class Topic
    {
        public int TopicId { get; set; }

        public string TopicName { get; set; }

        public string TopicText { get; set; }

        public DateTime TopicDateTime { get; set; }

        public int UserId { get; set; }

        public virtual User Users { get; set; }
    }
}
