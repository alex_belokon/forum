﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.Core.Models
{
   public class User
    {
        public int UserId { get; set; }

        public string UserIdFromIdentity { get; set; }

        public string UserName { get; set; }

        public string UserLogin { get; set; }

        public string UserPass { get; set; }

        public virtual ICollection<Topic> Topics { get; set; }

    }
}
