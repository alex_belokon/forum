﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.Core.Models

{
   public class Message
    {
        public int MessageId { get; set; }

        public string MessageText { get; set; }

        public DateTime MessageDateTime { get; set; }

        public int TopicId { get; set; }

        public int UserId { get; set; }

        public virtual Topic Topics { get; set; }

        public virtual User Users { get; set; }
    }
}
