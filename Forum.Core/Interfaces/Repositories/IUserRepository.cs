﻿using Forum.Core.Models;

namespace Forum.Core.Interfaces.Repositories
{
    public interface IUserRepository : IRepositoryBase<User>
    {
        User Get(int id);
    }
}
