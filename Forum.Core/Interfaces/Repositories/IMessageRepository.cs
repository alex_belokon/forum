﻿using Forum.Core.Models;

namespace Forum.Core.Interfaces.Repositories
{
    public interface IMessageRepository : IRepositoryBase<Message>
    {
        Message Get(int id);
    }
}
