﻿using Forum.Core.Models;

namespace Forum.Core.Interfaces.Managers
{
    public interface IUserManager
    {
        User[] GetAllUsers();

        User GetUserById(int userId);

        int AddUser(string userIdFromIdentity, string userName, string userLogin, string userPass);

        void EditUser(int userId, string userIdFromIdentity, string userName, string userLogin, string userPass);

        void DeleteUser(int userId);

    }
}
