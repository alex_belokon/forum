﻿
using Forum.Core.Models;
using System;

namespace Forum.Core.Interfaces.Managers
{
    public interface ITopicManager
    {
        Topic[] GetAllTopics();

        Topic GetTopicById(int topicId);

        void AddTopic(string topicName, string topicText, DateTime topicDateTime, int userId);

        void EditTopic(int topicId, string topicName, string topicText, DateTime topicDateTime, int userId);

        void DeleteTopic(int topicId);
    }
}
