﻿
using Forum.Core.Models;
using System;

namespace Forum.Core.Interfaces.Managers
{
    public interface IMessageManager
    {
        Message[] GetAllMessages();

        Message GetMessageById(int messageId);

        void AddMessage(string messageText, DateTime messageDateTime, int topicId, int userId);

        void EditMessage(int messageId, string messageText, DateTime messageDateTime, int topicId, int userId);

        void DeleteMessage(int messageId);
    }
}
